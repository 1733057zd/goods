package com.goods.business.mapper;


import com.goods.common.model.business.Consumer;
import tk.mybatis.mapper.common.Mapper;

public interface ConsumerMapper extends Mapper<Consumer> {
}
