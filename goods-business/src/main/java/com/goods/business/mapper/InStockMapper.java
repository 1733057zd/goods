package com.goods.business.mapper;


import com.goods.common.model.business.InStock;
import com.goods.common.vo.business.InStockVO;
import tk.mybatis.mapper.common.Mapper;

import java.util.List;

public interface InStockMapper extends Mapper<InStock> {


    /**
     * 分页查询和条件
     * @param inStockVO
     * @return
     */
    List<InStockVO> selectPageList(InStockVO inStockVO);
}
