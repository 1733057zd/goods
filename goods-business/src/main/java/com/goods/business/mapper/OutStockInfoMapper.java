package com.goods.business.mapper;

import com.goods.common.model.business.OutStockInfo;
import tk.mybatis.mapper.common.Mapper;

public interface OutStockInfoMapper extends Mapper<OutStockInfo> {
}
