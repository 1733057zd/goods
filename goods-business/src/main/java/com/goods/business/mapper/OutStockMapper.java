package com.goods.business.mapper;


import com.goods.common.model.business.OutStock;
import com.goods.common.vo.business.OutStockVO;
import tk.mybatis.mapper.common.Mapper;

import java.util.List;

public interface OutStockMapper extends Mapper<OutStock> {
    /**
     * 条件查询+两表查询
     * @param outStockVO
     * @return
     */
    List<OutStockVO> findPageList(OutStockVO outStockVO);
}
