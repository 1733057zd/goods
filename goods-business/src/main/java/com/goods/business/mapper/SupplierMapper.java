package com.goods.business.mapper;

import com.goods.common.model.business.Supplier;
import tk.mybatis.mapper.common.Mapper;

public interface SupplierMapper extends Mapper<Supplier> {
}
