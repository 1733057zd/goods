package com.goods.business.service;

import com.goods.common.model.business.Consumer;
import com.goods.common.vo.system.PageVO;

import java.util.List;

public interface ConsumerService {

    /**
     * 分页查询+条件查询
     * @param pageNum
     * @param pageSize
     * @param consumer
     * @return
     */
    PageVO<Consumer> findConsumerList(Integer pageNum, Integer pageSize, Consumer consumer);

    /**
     * 修改-数据回显
     * @param consumerId
     * @return
     */
    Consumer findById(Long consumerId);

    /**
     * 修改-更新
     * @param consumerId
     * @return
     */
    void update(Long consumerId,Consumer consumer);

    /**
     * 删除
     * @param consumerId
     * @return
     */
    void delete(Long consumerId);

    /**
     * 添加
     * @param consumer
     */
    void add(Consumer consumer);

    /**
     * 查询所以消费者
     * @return
     */
    List<Consumer> findAll();
}
