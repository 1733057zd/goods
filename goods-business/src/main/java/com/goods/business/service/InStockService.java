package com.goods.business.service;


import com.goods.common.vo.business.InStockDetailVO;
import com.goods.common.vo.business.InStockVO;
import com.goods.common.vo.system.PageVO;

public interface InStockService {
    /**
     * 入库分页查询
     * @param pageNum
     * @param pageSize
     * @param inStockVO
     * @return
     */
    PageVO<InStockVO> findInStockList(Integer pageNum, Integer pageSize, InStockVO inStockVO);

    /**
     * 商品明细
     * @param inStockId
     * @return
     */
    InStockDetailVO findDetail(Long inStockId,Integer pageNum);

    /**
     * 放入回收站
     * @param inStockId
     */
    void remove(Long inStockId);

    /**
     * 删除
     * @param inStockId
     */
    void delete(Long inStockId);

    /**
     * 还原
     * @param inStockId
     */
    void backAndPublic(Long inStockId);

    /**
     * 入库
     * @param inStockVO
     */
    void addIntoStock(InStockVO inStockVO);
}
