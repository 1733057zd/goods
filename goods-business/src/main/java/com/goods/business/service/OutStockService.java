package com.goods.business.service;

import com.goods.common.vo.business.OutStockDetailVO;
import com.goods.common.vo.business.OutStockVO;
import com.goods.common.vo.system.PageVO;

public interface OutStockService {

    /**
     * 分页查询+条件查询
     * @param pageNum
     * @param pageSize
     * @param outStockVO
     * @return
     */
    PageVO<OutStockVO> findPageList(Integer pageNum, Integer pageSize, OutStockVO outStockVO);

    /**
     * 通过id获得出库订单明细
     * @param outStockId
     * @param pageNum
     * @return
     */
    OutStockDetailVO getDetail(Long outStockId, Integer pageNum);

    /**
     * 放入回收站
     * @param outStockId
     */
    void remove(Long outStockId);

    /**
     * 恢复-通过
     * @param outStockId
     */
    void backAndPublish(Long outStockId);

    /**
     * 删除
     * @param outStockId
     */
    void delete(Long outStockId);

    /**
     * 物资出库
     * @param outStockVO
     */
    void addOutStock(OutStockVO outStockVO);
}
