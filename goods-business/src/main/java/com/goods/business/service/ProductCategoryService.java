package com.goods.business.service;

import com.goods.common.model.business.ProductCategory;
import com.goods.common.vo.business.ProductCategoryTreeNodeVO;
import com.goods.common.vo.system.PageVO;

import java.util.List;

public interface ProductCategoryService {

    /**
     * 商品分页查询
     * @param pageNum
     * @param pageSize
     * @return
     */
    PageVO getCategoryTree(Integer pageNum, Integer pageSize);

    /**
     * 加载父级分类数据
     * @return
     */
    List<ProductCategoryTreeNodeVO> getParentCategoryTree();

    /**
     * 添加分类
     * @param productCategory
     */
    void add(ProductCategory productCategory);

    /**
     * 根据id查询
     * @param id
     * @return
     */
    ProductCategory findById(Long id);

    /**
     * 根据id修改
     * @param id
     * @param productCategory
     */
    void update(Long id, ProductCategory productCategory);

    /**
     * 根据id删除
     * @param id
     */
    boolean delete(Long id);
}
