package com.goods.business.service;

import com.goods.common.model.business.Product;
import com.goods.common.vo.business.ProductStockVO;
import com.goods.common.vo.business.ProductVO;
import com.goods.common.vo.system.PageVO;

import java.util.List;

public interface ProductService {
    /**
     * 分页查询  和 条件查询
     * @param pageNum
     * @param pageSize
     * @param product
     * @return
     */
    PageVO<ProductVO> findProductList(Integer pageNum, Integer pageSize, ProductVO productVO);

    /**
     * 修改-数据回显
     * @param id
     * @return
     */
    Product findById(Long id);

    /**
     * 修改--更新
     * @param id
     * @param product
     */
    void update(Long id, Product product);

    /**
     * 放入回收站
     * @param id
     */
    void remove(Long id);

    /**
     * 恢复/
     * @param id
     */
    void backAndPublish(Long id);


    /**
     * 删除
     * @param id
     */
    void delete(Long id);

    /**
     * 商品添加
     * @param product
     */
    void add(Product product);

    /**
     * 查询所以库存 + 条件查询
     * @param pageNum
     * @param pageSize
     * @param productVO
     * @return
     */
    PageVO<ProductStockVO> findProductStocks(Integer pageNum, Integer pageSize, ProductVO productVO);

    /**
     * 饼状图
     * @param pageNum
     * @param pageSize
     * @param productVO
     * @return
     */
    List<ProductStockVO> findAllStocks(Integer pageNum, Integer pageSize, ProductVO productVO);
}
