package com.goods.business.service;

import com.goods.common.model.business.Supplier;
import com.goods.common.vo.system.PageVO;

import java.util.List;

public interface SupplierService {
    /**
     * 分页查询+条件查询
     * @param
     * @param supplier
     * @return
     */
    PageVO<Supplier> findSupplierList(Integer pageNum, Integer pageSize, Supplier supplier);

    /**
     * 添加物资来源
     * @param supplier
     * @return
     */
    Boolean add(Supplier supplier);

    /**
     * 编辑 --回显数据
     * @param id
     * @return
     */
    Supplier findById(Long id);

    /**
     * 修改--更新
     * @param supplier
     */
    void update(Supplier supplier);

    /**
     * 根据id删除
     * @param id
     */
    void delete(Long id);

    /**
     * 查询所以供应商名单
     * @return
     */
    List<Supplier> findAll();
}
