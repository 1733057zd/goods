package com.goods.business.service.imp;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.goods.business.mapper.ConsumerMapper;
import com.goods.business.service.ConsumerService;
import com.goods.common.model.business.Consumer;
import com.goods.common.vo.system.PageVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tk.mybatis.mapper.entity.Example;

import java.util.Date;
import java.util.List;

@Service
@SuppressWarnings("all")
public class ConsumerServiceImpl implements ConsumerService {

    @Autowired
    private ConsumerMapper consumerMapper;

    /**
     * 分页查询+条件查询
     *
     * @param pageNum
     * @param pageSize
     * @param consumer
     * @return
     */
    @Override
    public PageVO<Consumer> findConsumerList(Integer pageNum, Integer pageSize, Consumer consumer) {
        PageHelper.startPage(pageNum,pageSize);

        Example example = new Example(Consumer.class);
        Example.Criteria criteria = example.createCriteria();

        if(consumer!=null && consumer.getName()!=null){
            criteria.andLike("name","%"+consumer.getName()+"%");
        }
        if(consumer!=null && consumer.getAddress()!=null){
            criteria.andLike("address","%"+consumer.getAddress()+"%");
        }
        if(consumer!=null && consumer.getContact()!=null){
            criteria.andLike("contact","%"+consumer.getContact()+"%");
        }

        List<Consumer> consumerList = consumerMapper.selectByExample(example);

        PageInfo<Consumer> consumerPageInfo = new PageInfo<>(consumerList);

        return new PageVO<>(consumerPageInfo.getTotal(),consumerList);
    }

    /**
     * 修改-数据回显
     *
     * @param consumerId
     * @return
     */
    @Override
    public Consumer findById(Long consumerId) {
        if(consumerId!=null){
            Consumer consumer = consumerMapper.selectByPrimaryKey(consumerId);
            return consumer;
        }
       return null;
    }

    /**
     * 添加
     *
     * @param consumer
     */
    @Override
    public void add(Consumer consumer) {
        if(consumer!=null){
            consumer.setCreateTime(new Date());
            consumer.setModifiedTime(new Date());
            consumerMapper.insert(consumer);

        }
    }

    /**
     * 修改-更新
     *
     * @param consumerId
     * @return
     */
    @Override
    public void update(Long consumerId,Consumer consumer) {

        if(consumer==null) return;
        consumerMapper.updateByPrimaryKey(consumer);


    }

    /**
     * 删除
     *
     * @param consumerId
     * @return
     */
    @Override
    public void delete(Long consumerId) {
        Consumer consumer = this.findById(consumerId);
        if(consumer!=null){
            consumerMapper.deleteByPrimaryKey(consumerId);
        }
    }

    /**
     * 查询所以消费者
     *
     * @return
     */
    @Override
    public List<Consumer> findAll() {
        return consumerMapper.selectAll();
    }
}
