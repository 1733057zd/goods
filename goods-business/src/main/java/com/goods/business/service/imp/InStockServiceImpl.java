package com.goods.business.service.imp;

import com.alibaba.fastjson.JSONObject;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.goods.business.mapper.*;
import com.goods.business.service.InStockService;
import com.goods.common.model.business.*;
import com.goods.common.vo.business.InStockDetailVO;
import com.goods.common.vo.business.InStockItemVO;
import com.goods.common.vo.business.InStockVO;
import com.goods.common.vo.business.SupplierVO;
import com.goods.common.vo.system.PageVO;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;
import tk.mybatis.mapper.entity.Example;

import java.util.*;

@Service
@SuppressWarnings("all")
public class InStockServiceImpl implements InStockService {

    @Autowired
    private InStockMapper inStockMapper;
    @Autowired
    private StockInfoMapper stockInfoMapper;

    @Autowired
    private ProductMapper productMapper;

    @Autowired
    private SupplierMapper supplierMapper;

    @Autowired
    private ProductStockMapper productStockMapper;

    /**
     * 入库分页查询
     *
     * @param pageNum
     * @param pageSize
     * @param inStock
     * @return
     */
    @Override
    public PageVO<InStockVO> findInStockList(Integer pageNum, Integer pageSize, InStockVO inStockVO) {

       PageHelper.startPage(pageNum,pageSize);

        //查询
        List<InStockVO> inStocksList = inStockMapper.selectPageList(inStockVO);

        PageInfo<InStockVO> pageInfo = new PageInfo<>(inStocksList);

        return  new PageVO<>(pageInfo.getTotal(),inStocksList);


    }

    /**
     * 商品明细
     *
     * @param inStockId
     * @return
     */
    @Override
    public InStockDetailVO findDetail(Long inStockId,Integer pageNum) {

        //查询in_stock
        InStock inStock = inStockMapper.selectByPrimaryKey(inStockId);

        List<InStockItemVO> inStockItemVOList=new ArrayList<>();
        //查询stockinfo
        if(inStock!=null){
            Example example1 = new Example(InStockInfo.class);
            Example.Criteria criteria1 = example1.createCriteria();

            criteria1.andEqualTo("inNum",inStock.getInNum());
            List<InStockInfo> inStockInfoList = stockInfoMapper.selectByExample(example1);

            //查询product
            if(!CollectionUtils.isEmpty(inStockInfoList)){
                for (InStockInfo inStockInfo : inStockInfoList) {
                    example1 = new Example(Product.class);
                   criteria1 = example1.createCriteria();

                    criteria1.andEqualTo("pNum",inStockInfo.getPNum());
                    Product product = productMapper.selectOneByExample(example1);
                    InStockItemVO inStockItemVO = new InStockItemVO();
                    BeanUtils.copyProperties(product,inStockItemVO);
                    inStockItemVO.setCount(inStockInfo.getProductNumber());

                    inStockItemVOList.add(inStockItemVO);
                }
            }
        }

        //查询supplier
        Example example = new Example(Supplier.class);
        Example.Criteria criteria = example.createCriteria();
        Supplier supplier = supplierMapper.selectByPrimaryKey(inStock.getSupplierId());
        SupplierVO supplierVO = new SupplierVO();
        BeanUtils.copyProperties(supplier,supplierVO);

        InStockDetailVO inStockDetailVO = new InStockDetailVO();
        inStockDetailVO.setItemVOS(inStockItemVOList);
        inStockDetailVO.setTotal(inStockItemVOList.size());
        inStockDetailVO.setSupplierVO(supplierVO);
        inStockDetailVO.setStatus(inStock.getStatus());

        return inStockDetailVO;
    }


    /**
     * 放入回收站
     *
     * @param inStockId
     */
    @Override
    public void remove(Long inStockId) {
        if(inStockId!=null){
            InStock inStock = inStockMapper.selectByPrimaryKey(inStockId);
            inStock.setStatus(1);
            inStockMapper.updateByPrimaryKey(inStock);
        }

    }

    /**
     * 删除
     *
     * @param inStockId
     */
    @Override
    public void delete(Long inStockId) {
        if(inStockId!=null){
            inStockMapper.deleteByPrimaryKey(inStockId);
        }
    }

    /**
     * 还原
     *
     * @param inStockId
     */
    @Override
    public void backAndPublic(Long inStockId) {
        if(inStockId!=null){
            InStock inStock = inStockMapper.selectByPrimaryKey(inStockId);
            inStock.setStatus(0);
            inStockMapper.updateByPrimaryKey(inStock);
        }
    }

    /**
     * 入库
     *
     * @param inStockVO
     */
    @Override
    @Transactional
    public void addIntoStock(InStockVO inStockVO) {
        if(inStockVO==null) return;

        //插入instock
        InStock inStock = new InStock();
        String inNum = UUID.randomUUID().toString().replaceAll("-", "");
        inStock.setInNum(inNum);
        inStock.setType(inStockVO.getType());
        inStock.setOperator("admin");//todo 未获取当前用户
        inStock.setRemark(inStockVO.getRemark());
        inStock.setStatus(2);//待审核
        inStock.setCreateTime(new Date());
        inStock.setModified(new Date());


        //插入instock_info
        List<Object> products = inStockVO.getProducts();

        Integer productId=null;
        Integer productNumber=null;
        int productNumbers=0;
        if(!CollectionUtils.isEmpty(products)){
            for (Object product : products) {
                //获取productId  productNumber
                Map map = JSONObject.parseObject(JSONObject.toJSONString(product), Map.class);
                productId = (Integer) map.get("productId");
                productNumber = (Integer) map.get("productNumber");

                productNumbers+=productNumber.intValue();
                InStockInfo inStockInfo = new InStockInfo();
                inStockInfo.setInNum(inNum);
                inStockInfo.setProductNumber(productNumber);
                //获取pNum
                Product product1 = productMapper.selectByPrimaryKey(productId);
                if(product1!=null){
                    inStockInfo.setPNum(product1.getPNum());
                }
                stockInfoMapper.insert(inStockInfo);

                //todo product—stock添加库存

                Example example = new Example(ProductStock.class);
                Example.Criteria criteria = example.createCriteria();
                criteria.andEqualTo("pNum",product1.getPNum());
                ProductStock productStock = productStockMapper.selectOneByExample(example);
                //判断是否已存在
                if(productStock!=null){
                    //存在 更新
                    productStock.setStock(productStock.getStock()+Long.valueOf(productNumber));
                    productStockMapper.updateByPrimaryKey(productStock);
                }else {
                    //不存在 添加
                     productStock = new ProductStock();
                    productStock.setStock(Long.valueOf(productNumber));
                    productStock.setPNum(inNum);
                    productStockMapper.insert(productStock);
                }
            }
        }

        //数据数据加和后添加
        inStock.setProductNumber(productNumbers);

        //插入supplier
        //判断supplier是否存在
        Supplier supplier = supplierMapper.selectByPrimaryKey(inStockVO.getSupplierId());
        if(supplier==null){
            supplier = new Supplier();
            supplier.setId(inStockVO.getSupplierId());
            supplier.setName(inStockVO.getName());
            supplier.setAddress(inStockVO.getAddress());
            supplier.setPhone(inStockVO.getPhone());
            supplier.setContact(inStockVO.getContact());
            supplier.setEmail(inStockVO.getEmail());
            supplier.setSort(inStockVO.getSort());
            supplier.setCreateTime(new Date());
            supplier.setModifiedTime(new Date());

            supplierMapper.insert(supplier);

        }

        //如果新增则没有supplierid
        inStock.setSupplierId(supplier.getId());
        //最好在添加
        inStockMapper.insert(inStock);




    }
}
