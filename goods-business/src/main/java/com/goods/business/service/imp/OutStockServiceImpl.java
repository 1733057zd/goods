package com.goods.business.service.imp;

import com.alibaba.fastjson.JSONObject;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.goods.business.mapper.*;
import com.goods.business.service.OutStockService;
import com.goods.common.model.business.*;
import com.goods.common.vo.business.ConsumerVO;
import com.goods.common.vo.business.OutStockDetailVO;
import com.goods.common.vo.business.OutStockItemVO;
import com.goods.common.vo.business.OutStockVO;
import com.goods.common.vo.system.PageVO;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;
import tk.mybatis.mapper.entity.Example;

import java.util.*;

@Service
@SuppressWarnings("all")
public class OutStockServiceImpl implements OutStockService {


    @Autowired
    private OutStockMapper outStockMapper;

    @Autowired
    private ConsumerMapper consumerMapper;

    @Autowired
    private OutStockInfoMapper outStockInfoMapper;

    @Autowired
    private ProductMapper productMapper;

    @Autowired
    private ProductStockMapper productStockMapper;


    /**
     * 物资出库
     *
     * @param outStockVO
     */
    @Override
    @Transactional
    public void addOutStock(OutStockVO outStockVO) {

        //生成出库单号
        String outNum = UUID.randomUUID().toString().substring(4, 31);

        //封装outStock
        OutStock outStock = new OutStock();
        outStock.setOutNum(outNum);
        outStock.setType(outStockVO.getType());
        outStock.setStatus(2);
        outStock.setOperator("admin");
        outStock.setConsumerId(outStockVO.getConsumerId());
        outStock.setRemark(outStockVO.getRemark());
        outStock.setPriority(outStockVO.getPriority());
        outStock.setCreateTime(new Date());

        //商品数量product_number
        int productNumbers=0;

        //封装outStockInfo
        //  product_number
        List<Object> products = outStockVO.getProducts();
        if(!CollectionUtils.isEmpty(products)){
            for (Object product1 : products) {
                //获取productId 和 productNumber
                Map map = JSONObject.parseObject(JSONObject.toJSONString(product1), Map.class);
                Integer productId=(Integer)map.get("productId");
                Integer productNumber=(Integer)map.get("productNumber");
                OutStockInfo outStockInfo = new OutStockInfo();

                //组装outStockInfo数据
                outStockInfo.setOutNum(outNum);
                outStockInfo.setProductNumber(productNumber);
                outStockInfo.setCreateTime(new Date());
                outStockInfo.setModifiedTime(new Date());

                //查询product
                Product product = productMapper.selectByPrimaryKey(productId);
                if(product!=null) {
                    outStockInfo.setPNum(product.getPNum());
                }

                //添加outStockInfo
                outStockInfoMapper.insert(outStockInfo);

                //总数加和
                productNumbers+=productNumber.intValue();

                //同步库存数据
                Example example = new Example(ProductStock.class);
                Example.Criteria criteria = example.createCriteria();
                criteria.andEqualTo("pNum",product.getPNum());
                ProductStock productStock = productStockMapper.selectOneByExample(example);
                if(productStock!=null){
                    productStock.setStock(productStock.getStock()-productNumber.longValue());
                    productStockMapper.updateByPrimaryKey(productStock);
                }
            }

        }

        //组装ProductNumber
        outStock.setProductNumber(productNumbers);


        //封装consumer  判断是否存在
        Consumer consumer = consumerMapper.selectByPrimaryKey(outStockVO.getConsumerId());
        if(consumer==null){
            //消费者不存在  添加
            consumer = new Consumer();
            consumer.setName(outStockVO.getName());
            consumer.setAddress(outStockVO.getAddress());
            consumer.setPhone(outStockVO.getPhone());
            consumer.setSort(outStockVO.getSort());
            consumer.setContact(outStockVO.getContact());
            consumer.setCreateTime(new Date());
            consumer.setModifiedTime(new Date());

            //添加
            consumerMapper.insert(consumer);

        }

        outStock.setConsumerId(consumer.getId());
        //出库订单添加
        outStockMapper.insert(outStock);
    }

    /**
     * 通过id获得出库订单明细
     *
     * @param outStockId
     * @param pageNum
     * @return
     */
    @Override
    public OutStockDetailVO getDetail(Long outStockId, Integer pageNum) {

        OutStockDetailVO outStockDetailVO = new OutStockDetailVO();

        OutStock outStock = outStockMapper.selectByPrimaryKey(outStockId);
        if(outStock==null) return null;

        //状态
        outStockDetailVO.setStatus(outStock.getStatus());

        //消费者
        Example example = new Example(Consumer.class);
        Example.Criteria criteria = example.createCriteria();
        criteria.andEqualTo("id",outStock.getConsumerId());
        Consumer consumer = consumerMapper.selectOneByExample(example);
        if(consumer!=null) {
            ConsumerVO consumerVO = new ConsumerVO();
            BeanUtils.copyProperties(consumer,consumerVO);
            outStockDetailVO.setConsumerVO(consumerVO);
        }

        //总数

        //商品明细
        List<OutStockItemVO> outStockItemVOList=new ArrayList<>();

        example=new Example(OutStockInfo.class);
        criteria=example.createCriteria();
        criteria.andEqualTo("outNum",outStock.getOutNum());
        List<OutStockInfo> outStockInfoList = outStockInfoMapper.selectByExample(example);
        //封装具体明细
        if(!CollectionUtils.isEmpty(outStockInfoList)){
            example=new Example(Product.class);
            for (OutStockInfo outStockInfo : outStockInfoList) {
                criteria=example.createCriteria();
                criteria.andEqualTo("pNum",outStockInfo.getPNum());
                Product product = productMapper.selectOneByExample(example);

                if(product!=null){
                    OutStockItemVO outStockItemVO = new OutStockItemVO();
                    outStockItemVO.setName(product.getName());
                    outStockItemVO.setPNum(product.getPNum());
                    outStockItemVO.setModel(product.getModel());
                    outStockItemVO.setImageUrl(product.getImageUrl());
                    outStockItemVO.setUnit(product.getUnit());
                    //数量
                    outStockItemVO.setCount(outStockInfo.getProductNumber());

                    //添加到集合
                    outStockItemVOList.add(outStockItemVO);
                }
            }
        }
        //添加明细
        outStockDetailVO.setItemVOS(outStockItemVOList);

        outStockDetailVO.setTotal(outStockInfoList.size());


        return outStockDetailVO;
    }

    /**
     * 放入回收站
     *
     * @param outStockId
     */
    @Override
    public void remove(Long outStockId) {
        OutStock outStock = outStockMapper.selectByPrimaryKey(outStockId);
        if(outStock!=null){
            outStock.setStatus(1);
            outStockMapper.updateByPrimaryKey(outStock);
        }
    }



    /**
     * 恢复-通过
     *
     * @param outStockId
     */
    @Override
    public void backAndPublish(Long outStockId) {
        OutStock outStock = outStockMapper.selectByPrimaryKey(outStockId);
        if(outStock!=null){
            outStock.setStatus(0);
            outStockMapper.updateByPrimaryKey(outStock);
        }
    }


    /**
     * 删除
     *
     * @param outStockId
     */
    @Override
    public void delete(Long outStockId) {
        if(outStockId!=null){
            outStockMapper.deleteByPrimaryKey(outStockId);
        }
    }


    /**
     * 分页查询+条件查询
     *
     * @param pageNum
     * @param pageSize
     * @param outStockVO
     * @return
     */
    @Override
    public PageVO<OutStockVO> findPageList(Integer pageNum, Integer pageSize, OutStockVO outStockVO) {
        PageHelper.startPage(pageNum,pageSize);

        List<OutStockVO> outStockVOList= outStockMapper.findPageList(outStockVO);

        PageInfo<OutStockVO> pageInfo = new PageInfo<>(outStockVOList);
        return new PageVO<>(pageInfo.getTotal(),outStockVOList);
    }


}
