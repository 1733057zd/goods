package com.goods.business.service.imp;

import com.goods.business.mapper.ProductCategoryMapper;
import com.goods.business.service.ProductCategoryService;
import com.goods.common.model.business.ProductCategory;
import com.goods.common.utils.CategoryTreeBuilder;
import com.goods.common.utils.ListPageUtils;
import com.goods.common.vo.business.ProductCategoryTreeNodeVO;
import com.goods.common.vo.system.PageVO;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;
import tk.mybatis.mapper.entity.Example;

import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

@Service
@SuppressWarnings("all")
public class ProductCategoryServiceImpl implements ProductCategoryService {

    @Autowired
    private ProductCategoryMapper productCategoryMapper;

    /**
     * 商品分页查询
     *
     * @param pageNum
     * @param pageSize
     * @return
     */
    @Override
    public PageVO<ProductCategoryTreeNodeVO> getCategoryTree(Integer pageNum, Integer pageSize) {

        //获得所以数据
        List<ProductCategory> productCategoryTreeNodeList = productCategoryMapper.selectAll();
        //转换成vo对象
        List<ProductCategoryTreeNodeVO> productCategoryTreeNodeVodeList = productCategoryTreeNodeList.stream().map(productCategory -> {
            ProductCategoryTreeNodeVO productCategoryTreeNodeVO = new ProductCategoryTreeNodeVO();
            BeanUtils.copyProperties(productCategory, productCategoryTreeNodeVO);
            return productCategoryTreeNodeVO;
        }).collect(Collectors.toList());

        //构建三级分类
        List<ProductCategoryTreeNodeVO> productCategoryTreeNodeVOS = CategoryTreeBuilder.build(productCategoryTreeNodeVodeList);

        //返回PageVo 分页查询/
        List<ProductCategoryTreeNodeVO> page = ListPageUtils.page(productCategoryTreeNodeVOS, pageSize, pageNum);
        return  new PageVO<>(productCategoryTreeNodeVOS.size(),page);
    }

    /**
     * 加载父级分类数据2
     *
     * @return
     */
    @Override
    public List<ProductCategoryTreeNodeVO> getParentCategoryTree() {
        //获得所以数据
        List<ProductCategory> productCategoryTreeNodeList = productCategoryMapper.selectAll();
        //转换成vo对象
        List<ProductCategoryTreeNodeVO> productCategoryTreeNodeVodeList = productCategoryTreeNodeList.stream().map(productCategory -> {
            ProductCategoryTreeNodeVO productCategoryTreeNodeVO = new ProductCategoryTreeNodeVO();
            BeanUtils.copyProperties(productCategory, productCategoryTreeNodeVO);
            return productCategoryTreeNodeVO;
        }).collect(Collectors.toList());

        //构建三级分类
        List<ProductCategoryTreeNodeVO> productCategoryTreeNodeVOS = CategoryTreeBuilder.build(productCategoryTreeNodeVodeList);

        //返回PageVo 分页查询
        return productCategoryTreeNodeVOS;
    }

    /**
     * 添加分类
     *
     * @param productCategoryVO
     */
    @Override
    public void add(ProductCategory productCategory) {

        productCategory.setCreateTime(new Date());
        productCategory.setModifiedTime(new Date());

        productCategoryMapper.insert(productCategory);
    }

    /**
     * 根据id查询
     *
     * @param id
     * @return
     */
    @Override
    public ProductCategory findById(Long id) {
        return productCategoryMapper.selectByPrimaryKey(id);
    }

    /**
     * 根据id修改
     *
     * @param id
     * @param productCategory
     */
    @Override
    public void update(Long id, ProductCategory productCategory) {
        productCategoryMapper.updateByPrimaryKey(productCategory);
    }

    /**
     * 根据id删除,有下级则不删除
     *
     * @param id
     */
    @Override
    public boolean delete(Long id) {

        Example example = new Example(ProductCategory.class);
        Example.Criteria criteria = example.createCriteria();
        criteria.andEqualTo("pid",id);

        List<ProductCategory> productCategories = productCategoryMapper.selectByExample(example);
        if(!CollectionUtils.isEmpty(productCategories)){
            return false;
        }
        productCategoryMapper.deleteByPrimaryKey(id);
        return true;
    }
}
