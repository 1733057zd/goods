package com.goods.business.service.imp;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.goods.business.mapper.ProductMapper;
import com.goods.business.mapper.ProductStockMapper;
import com.goods.business.service.ProductService;
import com.goods.common.model.business.Product;
import com.goods.common.model.business.ProductStock;
import com.goods.common.utils.ListPageUtils;
import com.goods.common.vo.business.ProductStockVO;
import com.goods.common.vo.business.ProductVO;
import com.goods.common.vo.system.PageVO;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;
import tk.mybatis.mapper.entity.Example;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

@Service
@SuppressWarnings("all")
public class ProductServiceImpl implements ProductService {

    @Autowired
    private ProductMapper productMapper;

    @Autowired
    private ProductStockMapper productStockMapper;
    /**
     * 分页查询  和 条件查询
     *
     * @param pageNum
     * @param pageSize
     * @param product
     * @return
     */
    @Override
    public PageVO<ProductVO> findProductList(Integer pageNum, Integer pageSize, ProductVO productVO) {

        PageHelper.startPage(pageNum,pageSize);

        //条件查询
        List<Product> productList = getProductsList(productVO);
        Example example;
        Example.Criteria criteria;

        List<ProductVO> productVOList = productList.stream().map(product1 -> {
            ProductVO productVO1 = new ProductVO();
            BeanUtils.copyProperties(product1, productVO1);
            return productVO1;
        }).collect(Collectors.toList());

       // List<ProductVO> page = ListPageUtils.page(productVOList, pageSize, pageNum);
        //return new PageVO<>(page.size(),page);
        PageInfo<Product> page = new PageInfo<>(productList);
        return new PageVO<>(page.getTotal(),productVOList);




    }

    /**
     * 修改-数据回显
     *
     * @param id
     * @return
     */
    @Override
    public Product findById(Long id) {
        Product product = productMapper.selectByPrimaryKey(id);
        if(product!=null){
            return product;
        }
        return null;
    }

    /**
     * 修改--更新
     *
     * @param id
     * @param product
     */
    @Override
    public void update(Long id, Product product) {
        if(product!=null){

            productMapper.updateByPrimaryKey(product);
        }
    }

    /**
     * 放入回收站
     *
     * @param id
     */
    @Override
    public void remove(Long id) {
        Product product = productMapper.selectByPrimaryKey(id);
        product.setStatus(1);

        productMapper.updateByPrimaryKey(product);
    }



    /**
     * 恢复
     *
     * @param id
     */
    @Override
    public void backAndPublish(Long id) {
        Product product = productMapper.selectByPrimaryKey(id);
        product.setStatus(0);

        productMapper.updateByPrimaryKey(product);
    }

    /**
     * 删除
     *
     * @param id
     */
    @Override
    public void delete(Long id) {
        if(id!=null){
            productMapper.deleteByPrimaryKey(id);
        }
    }

    /**
     * 商品添加
     *
     * @param product
     */
    @Override
    public void add(Product product) {
        if(product!=null){
            String pnum = UUID.randomUUID().toString();
            product.setPNum(pnum);
            product.setStatus(2);
            product.setCreateTime(new Date());
            product.setModifiedTime(new Date());

            Long[] categoryKeys = product.getCategoryKeys();
            if(categoryKeys!=null && categoryKeys.length>0 && categoryKeys[0]!=null){
                product.setOneCategoryId(categoryKeys[0]);
            }
            if(categoryKeys!=null && categoryKeys.length>0 && categoryKeys[1]!=null){
                product.setTwoCategoryId(categoryKeys[1]);
            }
            if(categoryKeys!=null && categoryKeys.length>0 && categoryKeys[2]!=null){
                product.setThreeCategoryId(categoryKeys[2]);
            }

            productMapper.insert(product);

        }
    }


    /**
     * 查询所以库存 + 条件查询
     *
     * @param pageNum
     * @param pageSize
     * @param productVO
     * @return
     */
    @Override
    public PageVO<ProductStockVO> findProductStocks(Integer pageNum, Integer pageSize, ProductVO productVO) {

        List<Product> productList = getProductsList(productVO);
        Example example;
        Example.Criteria criteria;

        List<ProductStockVO> productStockVOList=new ArrayList<>();

        if (!CollectionUtils.isEmpty(productList)) {
            for (Product product : productList) {
                //转换成ProductStockVO
                ProductStockVO productStockVO = new ProductStockVO();
                BeanUtils.copyProperties(product,productStockVO);

                //查询库存剩余
                example = new Example(ProductStock.class);
                criteria = example.createCriteria();
                criteria.andEqualTo("pNum",product.getPNum());
                ProductStock productStock = productStockMapper.selectOneByExample(example);
                if(productStock!=null){
                    //ProductStockVO 设置库存属性
                    productStockVO.setStock(productStock.getStock());
                }

                //添加到集合里
                productStockVOList.add(productStockVO);
            }

        }

        //分页
        List<ProductStockVO> page = ListPageUtils.page(productStockVOList, pageSize, pageNum);
        return  new PageVO<>(productStockVOList.size(),page);
    }




    /**
     * 饼状图
     *
     * @param pageNum
     * @param pageSize
     * @param productVO
     * @return
     */
    @Override
    public  List<ProductStockVO> findAllStocks(Integer pageNum, Integer pageSize, ProductVO productVO) {

        List<Product> productList = getProductsList(productVO);
        Example example;
        Example.Criteria criteria;

        List<ProductStockVO> productStockVOList=new ArrayList<>();

        if (!CollectionUtils.isEmpty(productList)) {
            for (Product product : productList) {
                //转换成ProductStockVO
                ProductStockVO productStockVO = new ProductStockVO();
                BeanUtils.copyProperties(product,productStockVO);

                //查询库存剩余
                example = new Example(ProductStock.class);
                criteria = example.createCriteria();
                criteria.andEqualTo("pNum",product.getPNum());
                ProductStock productStock = productStockMapper.selectOneByExample(example);
                if(productStock!=null){
                    //ProductStockVO 设置库存属性
                    productStockVO.setStock(productStock.getStock());
                }

                //添加到集合里
                productStockVOList.add(productStockVO);
            }

        }

        return  productStockVOList;
    }


    /**
     * 条件查询
     * @param productVO
     * @return
     */
    private List<Product> getProductsList(ProductVO productVO) {
        Example example = new Example(Product.class);
        Example.Criteria criteria = example.createCriteria();

        //查询条件

        if (productVO != null && productVO.getStatus() != null) {
            criteria.andEqualTo("status", productVO.getStatus());
        }

        if (productVO != null && productVO.getName() != null) {
            criteria.andLike("name", "%" + productVO.getName() + "%");
        }
        Long[] categoryKeys = productVO.getCategoryKeys();
        if (categoryKeys != null && categoryKeys.length > 0 && categoryKeys[0] != null) {
            criteria.andEqualTo("oneCategoryId", categoryKeys[0]);
        }
        if (categoryKeys != null && categoryKeys.length > 2 && categoryKeys[1] != null) {
            criteria.andEqualTo("twoCategoryId", categoryKeys[1]);
        }
        if (categoryKeys != null && categoryKeys.length > 3 && categoryKeys[2] != null) {
            criteria.andEqualTo("threeCategoryId", categoryKeys[2]);
        }

        return productMapper.selectByExample(example);
    }

}
