package com.goods.business.service.imp;

import com.goods.business.mapper.SupplierMapper;
import com.goods.business.service.SupplierService;
import com.goods.common.model.business.Supplier;
import com.goods.common.utils.ListPageUtils;
import com.goods.common.vo.system.PageVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tk.mybatis.mapper.entity.Example;

import java.util.Date;
import java.util.List;

@Service
@SuppressWarnings("all")
public class SupplierServiceImpl implements SupplierService {

    @Autowired
    private SupplierMapper supplierMapper;

    /**
     * 分页查询+条件查询
     *
     * @param page
     * @param supplier
     * @return
     */
    @Override
    public PageVO<Supplier> findSupplierList(Integer pageNum, Integer pageSize, Supplier supplier) {

       // PageHelper.startPage(pageNum,pageSize);
        Example example = new Example(Supplier.class);
        Example.Criteria criteria = example.createCriteria();

        if(supplier.getName()!=null && !"".equals(supplier.getName())){
            criteria.andLike("name","%"+supplier.getName()+"%");
        }
        if(supplier.getAddress()!=null ){
            criteria.andLike("address","%"+supplier.getAddress()+"%");
        }
        if(supplier.getContact()!=null ){
            criteria.andLike("contact","%"+supplier.getContact()+"%");
        }

        List<Supplier> supplierList = supplierMapper.selectByExample(example);
//        PageInfo<Supplier> supplierPageInfo = new PageInfo<>(supplierList);
//        return new PageVO<>(supplierPageInfo.getTotal(),supplierList);


        List<Supplier> page = ListPageUtils.page(supplierList, pageSize, pageNum);
        return  new PageVO<>(supplierList.size(),page);
    }

    /**
     * 添加物资来源
     *
     * @param supplier
     * @return
     */
    @Override
    public Boolean add(Supplier supplier) {
        if(supplier!=null){
            supplier.setCreateTime(new Date());
            supplier.setModifiedTime(new Date());
            supplierMapper.insert(supplier);
            return true;
        }

        return false;
    }

    /**
     * 编辑 --回显数据
     *
     * @param id
     * @return
     */
    @Override
    public Supplier findById(Long id) {
        Supplier supplier = supplierMapper.selectByPrimaryKey(id);
        if(supplier!=null){
            return supplier;
        }
        return null;
    }


    /**
     * 修改--更新
     *
     * @param supplier
     */
    @Override
    public void update(Supplier supplier) {
        if(supplier==null) return;

        supplierMapper.updateByPrimaryKey(supplier);
    }

    /**
     * 根据id删除
     *
     * @param id
     */
    @Override
    public void delete(Long id) {
        if(id!=null){
            supplierMapper.deleteByPrimaryKey(id);
        }
    }

    /**
     * 查询所以供应商名单
     *
     * @return
     */
    @Override
    public List<Supplier> findAll() {
        return supplierMapper.selectAll();
    }
}

