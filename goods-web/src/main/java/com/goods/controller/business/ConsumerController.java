package com.goods.controller.business;

import com.goods.business.service.ConsumerService;
import com.goods.common.model.business.Consumer;
import com.goods.common.response.ResponseBean;
import com.goods.common.vo.system.PageVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/business/consumer")
public class ConsumerController {

    @Autowired
    private ConsumerService consumerService;

        ///business/consumer/9/business/consumer/findAll

    /**
     * 查询所以消费者
     * @return
     */
    @GetMapping("/findAll")
    public ResponseBean findAll(){

        List<Consumer> consumerList =consumerService.findAll();
        return ResponseBean.success(consumerList);
    }

    /**
     * 添加
     * @param consumer
     * @return
     */
    @PostMapping("/add")
    public ResponseBean add(@RequestBody Consumer consumer){

        consumerService.add(consumer);
        return ResponseBean.success();
    }
    /**
     * 删除
     * @param consumerId
     * @return
     */
    @DeleteMapping("/delete/{consumerId}")
    public ResponseBean delete(@PathVariable Long consumerId){

        consumerService.delete(consumerId);
        return ResponseBean.success();
    }

    /**
     * 修改-更新
     * @param consumerId
     * @return
     */
    @PutMapping("/update/{consumerId}")
    public ResponseBean update(@PathVariable Long consumerId,@RequestBody Consumer consumer){

        consumerService.update(consumerId,consumer);
        return ResponseBean.success();
    }

    /**
     * 修改-数据回显
     * @param consumerId
     * @return
     */
    @GetMapping("/edit/{consumerId}")
    public ResponseBean edit(@PathVariable Long consumerId){

        Consumer consumer =consumerService.findById(consumerId);
        return ResponseBean.success(consumer);
    }



    /**
     * //business/consumer/findConsumerList?pageNum=1&pageSize=10&name=
     * 分页查询+条件查询
     * @param pageNum
     * @param pageSize
     * @param consumer
     * @return
     */
    @GetMapping("/findConsumerList")
    public ResponseBean findConsumerList(@RequestParam Integer pageNum, @RequestParam Integer pageSize, Consumer consumer){

        PageVO<Consumer> consumerPageVO =consumerService.findConsumerList(pageNum,pageSize,consumer);

        return ResponseBean.success(consumerPageVO);
    }


}
