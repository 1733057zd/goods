package com.goods.controller.business;

import com.goods.business.service.InStockService;
import com.goods.common.response.ResponseBean;
import com.goods.common.vo.business.InStockDetailVO;
import com.goods.common.vo.business.InStockVO;
import com.goods.common.vo.system.PageVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;

@RestController
@RequestMapping("/business/inStock")
public class InStockController {

    @Autowired
    private InStockService inStockService;

    //business/inStock/addIntoStock

    /**
     * 入库
     * @param inStockVO
     * @return
     */
    @PostMapping("/addIntoStock")
    public ResponseBean addIntoStock(@RequestBody InStockVO inStockVO, HttpServletRequest request){

        inStockService.addIntoStock(inStockVO);

        return ResponseBean.success();

    }


    /**
     * publish/117
     * 通过
     * @param InStockId
     * @return
     */
    @PutMapping("/publish/{InStockId}")
    public ResponseBean publish(@PathVariable Long InStockId){

        inStockService.backAndPublic(InStockId);
        return ResponseBean.success();
    }


    /**
     * delete/117
     * 删除
     * @param InStockId
     * @return
     */
    @GetMapping("/delete/{InStockId}")
    public ResponseBean delete(@PathVariable Long InStockId){

        inStockService.delete(InStockId);
        return ResponseBean.success();
    }

    /**
     * back/117
     * 还原
     * @param InStockId
     * @return
     */
    @PutMapping("/back/{InStockId}")
    public ResponseBean back(@PathVariable Long InStockId){

        inStockService.backAndPublic(InStockId);
        return ResponseBean.success();
    }


    /**
     * remove/117
     * 放入回收站
     * @param InStockId
     * @return
     */
    @PutMapping("/remove/{InStockId}")
    public ResponseBean remove(@PathVariable Long InStockId){

        inStockService.remove(InStockId);
        return ResponseBean.success();
    }

    /**
     * /detail/117?pageNum=1
     * 商品明细
     * @param InStockId
     * @param pageNum
     * @return
     */
    @GetMapping("/detail/{InStockId}")
    public ResponseBean detail(@PathVariable Long InStockId,Integer pageNum){

        InStockDetailVO inStockDetailVO = inStockService.findDetail(InStockId,pageNum);
        //


        return ResponseBean.success(inStockDetailVO);
    }


    /**
     * findInStockList?pageNum=1&pageSize=10&status=0
     * 入库分页查询
     * @param pageNum
     * @param pageSize
     * @param inStockVO
     * @return
     */
    @GetMapping("/findInStockList")
    public ResponseBean findInStockList( Integer pageNum, Integer pageSize,
                                        InStockVO inStockVO){
        PageVO<InStockVO> inStockPageVO= inStockService.findInStockList(pageNum,pageSize,inStockVO);


        return  ResponseBean.success(inStockPageVO);
    }




}
