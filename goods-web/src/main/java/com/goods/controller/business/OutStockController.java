package com.goods.controller.business;

import com.goods.business.service.OutStockService;
import com.goods.common.response.ResponseBean;
import com.goods.common.vo.business.OutStockDetailVO;
import com.goods.common.vo.business.OutStockVO;
import com.goods.common.vo.system.PageVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/business/outStock")
public class OutStockController {

    @Autowired
    private OutStockService outStockService;

    //www.localhost8989/business/outStock/addOutStock

    /**
     * 物资出库
     * @param outStockVO
     * @return
     */
    @PostMapping("/addOutStock")
    public ResponseBean addOutStock(@RequestBody OutStockVO outStockVO) {

        outStockService.addOutStock(outStockVO);

        return ResponseBean.success();

    }

    /**
     * 删除
     * @param outStockId
     * @return
     */
    @GetMapping("/delete/{outStockId}")
    public ResponseBean delete(@PathVariable Long outStockId){

        outStockService.delete(outStockId);

        return ResponseBean.success();
    }
    /**
     * publish
     * 审核通过
     */
    @PutMapping("/publish/{outStockId}")
    public ResponseBean publish(@PathVariable Long outStockId){

        outStockService.backAndPublish(outStockId);
        return ResponseBean.success();
    }


    /**
     * back
     * 恢复
     */
    @PutMapping("/back/{outStockId}")
    public ResponseBean back(@PathVariable Long outStockId){

        outStockService.backAndPublish(outStockId);
        return ResponseBean.success();
    }

    /**
     * remove
     * 放入回收站
     */
    @PutMapping("/remove/{outStockId}")
    public ResponseBean remove(@PathVariable Long outStockId){

        outStockService.remove(outStockId);
        return ResponseBean.success();
    }



    /**
     * 通过id获得出库订单明细
     * @param outStockId
     * @param pageNum
     * @return
     */
    @GetMapping("/detail/{outStockId}")
    public ResponseBean detail(@PathVariable Long outStockId,@RequestParam Integer pageNum){

        OutStockDetailVO outStockDetailVO=outStockService.getDetail(outStockId,pageNum);

        return ResponseBean.success(outStockDetailVO);
    }


    /**
     * /business/outStock/findOutStockList?pageNum=1&pageSize=10&status=0
     * 分页查询+条件查询
     * @param pageNum
     * @param pageSize
     * @param outStockVO
     * @return
     */
    @GetMapping("/findOutStockList")
    public ResponseBean findOutStockList(@RequestParam Integer pageNum, @RequestParam Integer pageSize, OutStockVO outStockVO){

        PageVO<OutStockVO> outStockVOPageVO=outStockService.findPageList(pageNum,pageSize,outStockVO);

        return ResponseBean.success(outStockVOPageVO);
    }
}
