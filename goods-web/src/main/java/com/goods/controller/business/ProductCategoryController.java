package com.goods.controller.business;

import com.goods.business.service.ProductCategoryService;
import com.goods.common.model.business.ProductCategory;
import com.goods.common.response.ResponseBean;
import com.goods.common.vo.business.ProductCategoryTreeNodeVO;
import com.goods.common.vo.system.PageVO;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/business/productCategory")
@Api(tags = "业务模块-物资类别接口")
public class ProductCategoryController {

    @Autowired
    private ProductCategoryService productCategoryService;

   //

    /**
     * /business/productCategory/delete/79
     * 根据id删除
     * @param id
     * @return
     */
    @DeleteMapping("/delete/{id}")
    public ResponseBean delete(@PathVariable Long id){

        boolean delete = productCategoryService.delete(id);
        if(delete){
            return ResponseBean.success();
        }else {
            Map map=new HashMap();
            map.put("errorMsg","该分类含有下级，无法删除");

            return ResponseBean.error(map);
        }

    }


    /**
     * business/productCategory/update/78
     * 根据id修改
     * @param id
     * @param productCategory
     * @return
     */
    @PutMapping("update/{id}")
    public ResponseBean update(@PathVariable Long id,@RequestBody ProductCategory productCategory){

        productCategoryService.update(id,productCategory);
        return ResponseBean.success();
    }


    /**
     * /business/productCategory/edit/33
     * 根据产品分类id修改  先查询
     * @param id
     * @return
     */
    @GetMapping("/edit/{id}")
    public ResponseBean edit(@PathVariable Long id){

        ProductCategory productCategory=productCategoryService.findById(id);
        return ResponseBean.success(productCategory);
    }


    /**
     *  add
     * 添加分类
     * @param productCategory
     * @return
     */
    @PostMapping("/add")
    public ResponseBean add(@RequestBody ProductCategory productCategory){

        productCategoryService.add(productCategory);

        return ResponseBean.success();
    }


    /**
     *  /business/productCategory/getParentCategoryTree
     * 加载父级分类数据
     * @return
     */
    @ApiOperation(value = "物资分页",notes = "分页查询")
    @GetMapping("getParentCategoryTree")
        public ResponseBean<List<ProductCategoryTreeNodeVO>> getParentCategoryTree(){

            List<ProductCategoryTreeNodeVO> parentCategoryTree=productCategoryService.getParentCategoryTree();
            return ResponseBean.success(parentCategoryTree);
        }




    /**
     * /business/productCategory/categoryTree?pageNum=1&pageSize=5
     * 商品分页查询
     * @param pageNum
     * @param pageSize
     * @return
     */
    @GetMapping("categoryTree")
    public ResponseBean<PageVO<ProductCategoryTreeNodeVO>> getCategoryTree(Integer pageNum, Integer pageSize){

        PageVO ProductCategoryTreeNodeVO=productCategoryService.getCategoryTree(pageNum,pageSize);
        return ResponseBean.success(ProductCategoryTreeNodeVO);
    }

}
