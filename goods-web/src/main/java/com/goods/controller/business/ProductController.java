package com.goods.controller.business;

import com.goods.business.service.ProductService;
import com.goods.common.model.business.Product;
import com.goods.common.response.ResponseBean;
import com.goods.common.vo.business.ProductStockVO;
import com.goods.common.vo.business.ProductVO;
import com.goods.common.vo.system.PageVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/business/product")
public class ProductController {

    @Autowired
    private ProductService productService;


    //business/product/findProductStocks?pageSize=9&pageNum=1&name=111

    //business/product/findAllStocks?pageSize=9&pageNum=1&name=11&categorys=20,43,57

    /**
     * 饼状图
     * @param pageSize
     * @param pageNum
     * @param productVO
     * @param categorys
     * @return
     */
    @GetMapping("/findAllStocks")
    public ResponseBean findAllStocks(Integer pageSize,Integer pageNum,ProductVO productVO,Long[] categorys){

        productVO.setCategoryKeys(categorys);
        List<ProductStockVO> productStockVOPageVO=productService.findAllStocks(pageNum,pageSize,productVO);

        return ResponseBean.success(productStockVOPageVO);
    }

    /**
     * 查询所以库存 + 条件查询+
     * @param pageSize
     * @param pageNum
     * @param productVO
     * @return
     */
    @GetMapping("/findProductStocks")
    public ResponseBean findProductStocks(Integer pageSize,Integer pageNum,ProductVO productVO,Long[] categorys){

        productVO.setCategoryKeys(categorys);
        PageVO<ProductStockVO> productStockVOPageVO=productService.findProductStocks(pageNum,pageSize,productVO);

        return ResponseBean.success(productStockVOPageVO);
    }

    /**
     * findProducts?pageNum=1&pageSize=6&status=0&name=%E4
     * @param pageNum
     * @param pageSize
     * @param productVO
     * @param categorys
     * @return
     */
    @GetMapping("/findProducts")
    public ResponseBean findProducts(@RequestParam Integer pageNum, @RequestParam Integer pageSize,ProductVO productVO,Long[] categorys){

        productVO.setCategoryKeys(categorys);
        PageVO<ProductVO> pageVO =productService.findProductList(pageNum,pageSize,productVO);

        return ResponseBean.success(pageVO);
    }

    /**
     * add
     * 商品添加
     * @param product
     * @return
     */
    @PostMapping("/add")
    public ResponseBean add(@RequestBody Product product){



        productService.add(product);

        return ResponseBean.success();
    }
    /**
     * delete/{id}
     * 删除
     * @param id
     * @return
     */
    @DeleteMapping("/delete/{id}")
    public ResponseBean delete(@PathVariable Long id){

        productService.delete(id);
        return ResponseBean.success();
    }


    /**
     * /publish/58
     *审核通过
     */
    @PutMapping("/publish/{id}")
    public ResponseBean publish(@PathVariable Long id){

        productService.backAndPublish(id);
        return ResponseBean.success();
    }

    /**
     * /back/56
     * 恢复
     * @param id
     * @return
     */
    @PutMapping("/back/{id}")
    public ResponseBean bace(@PathVariable Long id){

        productService.backAndPublish(id);

        return ResponseBean.success();
    }


    /**
     * remove/57
     * 放入回收站
     * @param id
     * @return
     */
    @PutMapping("remove/{id}")
    public ResponseBean remove(@PathVariable Long id){

        productService.remove(id);
        return ResponseBean.success();
    }

    /**
     * update/56
     * 修改--更新
     * @param id
     * @param product
     * @return
     */
    @PutMapping("/update/{id}")
    public ResponseBean upodate(@PathVariable Long id,@RequestBody Product product){

        productService.update(id,product);
        return ResponseBean.success();
    }
    /**
     * /edit/56
     * 修改-数据回显
     * @param id
     * @return
     */
    @GetMapping("/edit/{id}")
    public ResponseBean edit(@PathVariable Long id){

        Product product= productService.findById(id);

        return ResponseBean.success(product);
    }



    /**
     * /findProductList?pageNum=1&pageSize=6&name=&categoryId=&supplier=&status=0
     * 分页查询  和 条件查询
     * @param pageNum
     * @param pageSize
     * @param product
     * @return
     */
    @GetMapping("/findProductList")
    public ResponseBean findProductList(@RequestParam Integer pageNum, @RequestParam Integer pageSize,ProductVO productVO,Long[] categorys){

        productVO.setCategoryKeys(categorys);
        PageVO<ProductVO> pageVO =productService.findProductList(pageNum,pageSize,productVO);

        return ResponseBean.success(pageVO);
    }
}
