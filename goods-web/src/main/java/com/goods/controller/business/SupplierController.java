package com.goods.controller.business;

import com.goods.business.service.SupplierService;
import com.goods.common.model.business.Supplier;
import com.goods.common.response.ResponseBean;
import com.goods.common.vo.system.PageVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/business/supplier")
public class SupplierController {

    @Autowired
    private SupplierService supplierService;



    ///business/supplier/findAll

    /**
     * 查询所以供应商名单
     * @return
     */
    @GetMapping("/findAll")
    public ResponseBean findAll(){

        List<Supplier> suppliers =supplierService.findAll();
        return ResponseBean.success(suppliers);
    }

    /**
     * 根据id删除
     * @param id
     * @return
     */
    @DeleteMapping("/delete/{id}")
    public ResponseBean delete(@PathVariable Long id){

        supplierService.delete(id);
        return ResponseBean.success();
    }

    /**
     * /supplier/update/26
     * 修改--更新
     * @param id
     * @param supplier
     * @return
     */
    @PutMapping("/update/{id}")
    public ResponseBean update(@PathVariable Long id,@RequestBody Supplier supplier){

        supplierService.update(supplier);
        return ResponseBean.success();
    }

    /**
     * //edit/26
     * 编辑 --回显数据
     * @param id
     * @return
     */
    @GetMapping("/edit/{id}")
    public ResponseBean edit(@PathVariable Long id){

        Supplier supplier=supplierService.findById(id);
        if(supplier!=null){
        return ResponseBean.success(supplier);
        }
        return ResponseBean.error("错误");
    }


    /**
     * /business/supplier/add
     * 添加
     * @param supplier
     * @return
     */
    @PostMapping("/add")
    public ResponseBean add(@RequestBody Supplier supplier){

        Boolean result=supplierService.add(supplier);
        return ResponseBean.success();

    }

    /**
     * /findSupplierList?pageNum=1&pageSize=10&name=
     * 分页查询+条件查询
     * @param pageNum
     * @param pageSize
     * @return
     */
    @GetMapping("findSupplierList")
    public ResponseBean findSupplierList(Integer pageNum,
                                         Integer pageSize,
                                         String name,
                                         String address,
                                         String contcat){

        Supplier supplier = new Supplier();
        supplier.setName(name);
        supplier.setAddress(address);
        supplier.setContact(contcat);
        PageVO<Supplier> supplierList= supplierService.findSupplierList(pageNum,pageSize,supplier);
        // todo 无返回结果时返回提示map
        return ResponseBean.success(supplierList);
    }

}
