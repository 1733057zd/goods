package com.atguigu.juc;

import javax.sound.midi.Soundbank;
import java.sql.Time;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.TimeUnit;

public class BlockingQueueDemo {

    public static void main(String[] args) {
        ArrayBlockingQueue<Object> queue = new ArrayBlockingQueue<Object>(3);
/*        System.out.println(queue.add("q"));
        System.out.println(queue.add("a"));
        System.out.println(queue.add("a"));
//        System.out.println(queue.add("a"));
        System.out.println(queue.remove());
        System.out.println(queue.remove());
        System.out.println(queue.remove());
//        System.out.println(queue.remove());*/

/*        System.out.println(queue.offer("q"));
        System.out.println(queue.offer("q"));
        System.out.println(queue.offer("q"));
//        System.out.println(queue.offer("q"));
        System.out.println(queue.poll());
        System.out.println(queue.poll());
//        System.out.println(queue.poll());
//        System.out.println(queue.poll());
        System.out.println(queue.peek());*/

/*
        try {
            queue.put("aa");
            queue.put("aa");
            queue.put("aa");
           // queue.put("aa");
            System.out.println(queue.take());
            System.out.println(queue.take());
            System.out.println(queue.take());
            System.out.println(queue.take());




        } catch (InterruptedException e) {
            e.printStackTrace();
        }*/

        try {
            System.out.println(queue.offer("aa",1, TimeUnit.SECONDS));
            System.out.println(queue.offer("aa",1, TimeUnit.SECONDS));
            System.out.println(queue.offer("aa",1, TimeUnit.SECONDS));
         //   System.out.println(queue.offer("aa",10, TimeUnit.SECONDS));

            System.out.println(queue.poll(1,TimeUnit.SECONDS));
            System.out.println(queue.poll(1,TimeUnit.SECONDS));
            System.out.println(queue.poll(1,TimeUnit.SECONDS));
            System.out.println(queue.poll(1,TimeUnit.SECONDS));
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

}
