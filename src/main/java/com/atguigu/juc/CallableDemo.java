package com.atguigu.juc;

import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.FutureTask;

class MyCallableThread implements Callable<Integer>{

    @Override
    public Integer call() throws Exception {
        System.out.println("执行了callable");
        return 200;
    }
}

public class CallableDemo {

    public static void main(String[] args) {

        MyCallableThread callableThread = new MyCallableThread();
        FutureTask<Integer> futureTask = new FutureTask<Integer>(callableThread);

        new Thread(futureTask).start();
        try {
            System.out.println(futureTask.get());
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        }


    }
}
