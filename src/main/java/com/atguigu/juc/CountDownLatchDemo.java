package com.atguigu.juc;


import javax.activation.DataSource;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;

/**
 * 倒计时器
 */
public class CountDownLatchDemo {

    public static void main(String[] args) {

        CountDownLatch downLatch = new CountDownLatch(6);

        for (int i = 0; i < 60; i++) {

            new Thread(()->{
                try {
                    TimeUnit.SECONDS.sleep(3);
                    System.out.println("第"+ Thread.currentThread().getName() +"位");
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            },String.valueOf(i+1)).start();

            downLatch.countDown();
        }

        try {
            downLatch.await();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

    }
}
