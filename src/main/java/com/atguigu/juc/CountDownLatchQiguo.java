package com.atguigu.juc;


import java.util.ArrayList;
import java.util.concurrent.CountDownLatch;

public class CountDownLatchQiguo {

    public static void main(String[] args)  {

        CountDownLatch countDownLatch = new CountDownLatch(6);

        ArrayList<String> list = new ArrayList<>();
        list.add("齐");
        list.add("楚");
        list.add("燕");
        list.add("韩");
        list.add("赵");
        list.add("魏");

        for (java.lang.String country : list) {
            new Thread(()->{
                System.out.println("秦国消灭了"+ country + "国");
                countDownLatch.countDown();
            },country).start();
        }

        try {
            countDownLatch.await();
            System.out.println("六王毙，四海一");
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

    }
}
