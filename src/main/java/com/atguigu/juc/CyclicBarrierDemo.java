package com.atguigu.juc;

import java.util.concurrent.BrokenBarrierException;
import java.util.concurrent.CyclicBarrier;
import java.util.concurrent.TimeUnit;

/**
 * 循环栅栏
 */

public class CyclicBarrierDemo {

    public static void main(String[] args) {

        CyclicBarrier cyclicBarrier = new CyclicBarrier(3, () -> {
            System.out.println(Thread.currentThread().getName()+"打通关了");
        });

        for (int i = 0; i < 3; i++) {

            new Thread(()->{

                try {
                    System.out.println(Thread.currentThread().getName()+"打第一关");
                    TimeUnit.SECONDS.sleep(2);
                    System.out.println(Thread.currentThread().getName()+"打死了boss");
                    cyclicBarrier.await();

                    System.out.println(Thread.currentThread().getName()+"打第二关");
                    TimeUnit.SECONDS.sleep(2);
                    System.out.println(Thread.currentThread().getName()+"打死了boss");
                    cyclicBarrier.await();

                    System.out.println(Thread.currentThread().getName()+"打第三关");
                    TimeUnit.SECONDS.sleep(2);
                    System.out.println(Thread.currentThread().getName()+"打死了boss");
                    cyclicBarrier.await();

                } catch (InterruptedException e) {
                    e.printStackTrace();
                } catch (BrokenBarrierException e) {
                    e.printStackTrace();
                }

            },String.valueOf(i+1)).start();
        }
    }
}
