package com.atguigu.juc;

class MyRunnable implements Runnable{

    @Override
    public void run() {
        System.out.println(Thread.currentThread().getName() + "执行了");
    }
}


public class DemoRunnable {

    public static void main(String[] args) {

        //普通
        Thread thread = new Thread(new MyRunnable(),"线程二");
        thread.start();

        //匿名内部类
        new Thread(new Runnable() {
            @Override
            public void run() {
                System.out.println(Thread.currentThread().getName() + "执行了");
            }
        },"线程三").start();

        //lambda
        //拷贝（），写死-> ,{}
        new Thread(()->{
            System.out.println(Thread.currentThread().getName() + "执行了");
        },"线程四").start();



    }

}
