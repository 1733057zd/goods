package com.atguigu.juc;

/*class Demo{
    public synchronized void aa(){
        System.out.println("aaaaaaaaaa");
        bb();
    }

    public synchronized void bb() {
        System.out.println("bbbbbbbbbbbbbb");
    }

}*/


import java.util.concurrent.locks.ReentrantLock;

class Demo{

    ReentrantLock lock = new ReentrantLock();

    public void aa(){
        lock.lock();
        try {
            System.out.println("aaaaaaaa");
            bb();
        } finally {
            lock.unlock();
        }
    }

    public void bb(){
        lock.lock();
        try {
            System.out.println("bbbbbbbb");
        } finally {
            lock.unlock();
        }
    }

}


public class DemoTest {

    public static void main(String[] args) {

        Demo demo = new Demo();
        demo.aa();
    }
}
