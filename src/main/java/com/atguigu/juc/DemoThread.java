package com.atguigu.juc;

class MyThread extends Thread{

    @Override
    public void run() {
        System.out.println(currentThread().getName()+"执行了");
        System.out.println(Thread.currentThread().getName()+"执行了");

    }
}



public class DemoThread {

    public static void main(String[] args) {

        MyThread myThread = new MyThread();
        myThread.setName("线程一");
        myThread.start();

    }
}
