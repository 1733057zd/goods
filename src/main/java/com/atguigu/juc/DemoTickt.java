package com.atguigu.juc;

import sun.nio.cs.ext.TIS_620;

import java.util.concurrent.locks.ReentrantLock;

class Ticket{
    private Integer number = 20;
    ReentrantLock lock = new ReentrantLock(true);

    public void sale(){
        lock.lock();

        if(number<=0){
            System.out.println("卖完了......");
            return;
        }

        try {
            Thread.sleep(200);
            number--;
            System.out.println(Thread.currentThread().getName()+"线程卖出了票：当前剩余："+ number);

        } catch (InterruptedException e) {
            e.printStackTrace();
        }finally {
            lock.unlock();
        }
    }
}

public class DemoTickt {
    public static void main(String[] args) {

        Ticket ticket = new Ticket();

        new Thread(()->{
            for (int i = 0; i < 30; i++) {
                ticket.sale();
            }
        },"aa").start();

        new Thread(()->{
            for (int i = 0; i < 30; i++) {

            ticket.sale();
            }
        },"bb").start();

    }

}
