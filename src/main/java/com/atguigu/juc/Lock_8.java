package com.atguigu.juc;

import java.awt.*;
import java.util.regex.Pattern;

class Phone{

    //非静态代码
   // public synchronized void sendSMS() throws InterruptedException {
    public void sendSMS() throws InterruptedException {

        synchronized (this){
            Thread.sleep(4000);
            System.out.println("sendSMS...................");
        }

    }


    public  synchronized void sendEmail(){
        synchronized (this){

        System.out.println("sendEmail................");
        }
    }

    public void sayHello(){
        System.out.println("sayHello.........");
    }
}


public class Lock_8 {

    public static void main(String[] args) {

        Phone phone1 = new Phone();
        Phone phone2 = new Phone();

        new Thread(()->{

            try {
                phone1.sendSMS();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        },"aa").start();


        new Thread(()->{

                phone1.sendEmail();
//            phone1.sayHello();
//            phone2.sendEmail();
        },"bb").start();
    }

}
