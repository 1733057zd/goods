package com.atguigu.juc;

import sun.applet.Main;

import java.rmi.server.UID;
import java.sql.Connection;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.concurrent.CopyOnWriteArraySet;

public class NotSafeDemo {

/*    public static void main(String[] args) {

        //List<String> list = new ArrayList<>();
        //List<String> list=new Vector<>();
//        List<String> list= Collections.synchronizedList(new ArrayList<>());
        List<String> list=new CopyOnWriteArrayList<>();
        for (int i = 0; i < 20; i++) {

            new Thread(()->{
                list.addU(UID.randomUUID().toString().substring(0,8));
                System.out.println(list);
            },String.valueOf(i)).start();
        }
    }*/
/*    public static void main(String[] args) {

        //HashMap<String, String> map = new HashMap<>();
        Map<String, String> map = new ConcurrentHashMap<>();
        for (int i = 0; i < 20; i++) {
            new Thread(()->{
                map.put(String.valueOf(Thread.currentThread().getName()), UUID.randomUUID().toString().substring(0,8));
                System.out.println(map);
            },String.valueOf(i)).start();
        }
    }*/

    public static void main(String[] args) {

        //List<String> list = new ArrayList<>();
        //List<String> list=new Vector<>();
//        List<String> list= Collections.synchronizedList(new ArrayList<>());
        Set<String> set=new CopyOnWriteArraySet<>();
        for (int i = 0; i < 20; i++) {

            new Thread(()->{
                set.add(UUID.randomUUID().toString().substring(0,8));
                System.out.println(set);
            },String.valueOf(i)).start();
        }
    }
}
