package com.atguigu.juc;

class shareDataOne {
    private Integer num = 0;

    public synchronized void add() throws InterruptedException {
        //1 判断条件
        if (num != 0) {
            this.wait();
        }
        //2 操作
        num++;
        System.out.println(Thread.currentThread().getName()+":" +num);
        //通知
        this.notifyAll();
    }

    public synchronized void dec() throws InterruptedException {
        //1 判断条件
        if(num!=1){
            this.wait();
        }

        //2 操作
        num--;
        System.out.println(Thread.currentThread().getName()+":" +num);

        //3 通知
        this.notifyAll();
    }
}

public class NotifyWaitDemo {
    public static void main(String[] args) {

        shareDataOne shareDataOne = new shareDataOne();

        new Thread(()->{
            for (int i = 0; i < 10; i++) {
                try {
                    shareDataOne.add();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }

        },"aaa").start();

        new Thread(()->{
            for (int i = 0; i < 10; i++) {
                try {
                    shareDataOne.dec();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }

        },"bb").start();
    }
}
