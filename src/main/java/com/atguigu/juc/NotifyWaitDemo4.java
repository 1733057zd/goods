package com.atguigu.juc;

import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

class shareDataOne4 {
    private Integer num = 0;

    final  Lock lock=new ReentrantLock();
    final Condition condition=lock.newCondition();

    public  void add() throws InterruptedException {
        lock.lock();

        try {
            //1 判断条件
            while (num != 0) {
               condition.await();
            }
            //2 操作
            num++;
            System.out.println(Thread.currentThread().getName()+":" +num);
            //通知
            condition.signalAll();
        } finally {
            lock.unlock();
        }
    }

    public  void dec() throws InterruptedException {
        lock.lock();

        try {
            //1 判断条件
            while (num!=1){
                condition.await();
            }

            //2 操作
            num--;
            System.out.println(Thread.currentThread().getName()+":" +num);

            //3 通知
            condition.signalAll();
        } finally {
            lock.unlock();
        }
    }
}

public class NotifyWaitDemo4 {
    public static void main(String[] args) {

        shareDataOne4 shareDataOne4 = new shareDataOne4();

        new Thread(()->{
            for (int i = 0; i < 10; i++) {
                try {
                    shareDataOne4.add();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }

        },"aaa").start();

        new Thread(()->{
            for (int i = 0; i < 10; i++) {
                try {
                    shareDataOne4.dec();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }

        },"bb").start();

    }
}
