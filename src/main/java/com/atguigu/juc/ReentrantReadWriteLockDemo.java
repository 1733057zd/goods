package com.atguigu.juc;


import org.junit.Test;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.locks.ReentrantReadWriteLock;

class MyCache{
    private volatile  Map<String,String> map = new HashMap<>();
    ReentrantReadWriteLock rrwlock = new ReentrantReadWriteLock(true);

    public void put(String key,String value){
        //开始写入
        rrwlock.writeLock().lock();
        try {
            System.out.println(Thread.currentThread().getName()+"开始写入"+key);
            Thread.sleep(200);
            System.out.println(Thread.currentThread().getName()+"写入成功"+key);

        } catch (InterruptedException e) {
            e.printStackTrace();
        }finally {
            rrwlock.writeLock().unlock();
        }
    }

    public void get(String key){
        rrwlock.readLock().lock();
        try {
            //开始读入
            System.out.println(Thread.currentThread().getName()+"开始读入"+key);
            Thread.sleep(200);
            System.out.println(Thread.currentThread().getName()+"读入成功"+key);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }finally {
            rrwlock.readLock().unlock();
        }
    }
}

public class ReentrantReadWriteLockDemo {
    public static void main(String[] args) {

        MyCache myCache = new MyCache();

        new Thread(()->{
            for (int i = 0; i < 10; i++) {
                String string = String.valueOf(i);
                myCache.put(string,string);
            }
        },"线程1").start();

        new Thread(()->{
            for (int i = 0; i <20; i++) {
                String s = String.valueOf(i);
                myCache.get(s);
            }
        },"线程2").start();
    }

    @Test
    public void test(){

    }
}

