package com.atguigu.juc;


import java.util.concurrent.Semaphore;
import java.util.concurrent.TimeUnit;

/**
 * 信号量问题
 * 3个车位（信号量）
 * 6个车（线程）
 */
public class SemaphoreDemo {

    public static void main(String[] args) {

        Semaphore semaphore = new Semaphore(3);

        for (int i = 0; i < 6; i++) {

            new Thread(()->{

                try {
                    //获取一个资源
                    semaphore.acquire();
                    System.out.println(Thread.currentThread().getName()+"进来");
                    TimeUnit.SECONDS.sleep(1);
                    System.out.println(Thread.currentThread().getName()+"离开");
                    //释放一个资源
                    semaphore.release();

                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            },String.valueOf(i+1)).start();
        }
    }

}
