package com.atguigu.juc;

import com.sun.corba.se.impl.orbutil.CorbaResourceUtil;

import java.util.concurrent.Executor;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class ThreadPoolDemo {

    public static void main(String[] args) {

        //ExecutorService threadPool = Executors.newCachedThreadPool();
        //ExecutorService threadPool = Executors.newFixedThreadPool(3);
        ExecutorService threadPool = Executors.newSingleThreadExecutor();
        for (int i = 0; i < 50; i++) {
            threadPool.execute(()->{
                System.out.println(Thread.currentThread().getName()+"执行了该线程");
            });
        }
    }
}
