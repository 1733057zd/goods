package com.atguigu.juc;

import java.sql.Time;
import java.util.concurrent.*;

public class ThreadPoolDemo2 {

    public static void main(String[] args) {
        ThreadPoolExecutor poolExecutor = new ThreadPoolExecutor(5, 10, 10, TimeUnit.SECONDS,
                new ArrayBlockingQueue<>(100), Executors.defaultThreadFactory(),
                //new ThreadPoolExecutor.AbortPolicy()
                //new ThreadPoolExecutor.CallerRunsPolicy()
                new ThreadPoolExecutor.DiscardOldestPolicy()
                /*new RejectedExecutionHandler() {
                    @Override
                    public void rejectedExecution(Runnable r, ThreadPoolExecutor executor) {
                        System.out.println("自定义拒绝策略..................");
                    }
                }*/);

        for (int i = 0; i < 100; i++) {
            poolExecutor.execute(()->{
                System.out.println(Thread.currentThread().getName()+"执行了");
            });
        }
    }


}
